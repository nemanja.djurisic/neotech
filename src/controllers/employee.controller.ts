import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Res,NotFoundException, Query } from "@nestjs/common";
import { Employee } from "src/models/employee.schema";
import { EmployeeService } from "src/services/employee.service";

@Controller('/api/employee')
export class EmployeeController {

    constructor(private readonly employeeService: EmployeeService) { }


    @Post('/create')
    async addEmployee(@Res() response, @Body() employee: Employee) {
        const newEmployee = await this.employeeService.addEmployee(employee);
        return response.status(HttpStatus.OK).json({
            message: "Employee has been created successfully",
            newEmployee
        })
    }

    @Get('employees')
    async getAllEmployees(@Res() response) {
        const employees = await this.employeeService.getAllEmployee();
        return response.status(HttpStatus.OK).json(employees);
    }

    @Get('employee/:employeeId')
    async getEmployee(@Res() response, @Param('employeeId') employeeId) {
        const employee = await this.employeeService.getEmployee(employeeId);

        if (!employee) {
            throw new NotFoundException('Employee does not exists!');
        }

        return response.status(HttpStatus.OK).json(employee);
    }

    @Put('/update')
    async updateEmployee(@Res() response, @Query('employeeId') employeeId, @Body() employee: Employee) {
        const newEmployee = await this.employeeService.updateEmployee(employeeId, employee);
        if (!newEmployee) {
            throw new NotFoundException('Employee does not exists!');
        }

        return response.status(HttpStatus.OK).json({
            message: 'Employee has been updated successfully!',
            newEmployee
        });
    }

    @Delete('/delete')
    async deleteEmployee(@Res() response, @Query('employeeId') employeeId) {
        const employee = await this.employeeService.deleteEmployee(employeeId);

        if (!employee) {
            throw new NotFoundException('Employee does not exists!');
        }

        return response.status(HttpStatus.OK).json({
            message: 'Employee has been successfully deleted',
            employee
        })
    }
}