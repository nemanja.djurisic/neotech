import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Employee, EmployeeDocument } from "src/models/employee.schema";

@Injectable()
export class EmployeeService {
    constructor(@InjectModel(Employee.name) private employeeModel: Model<EmployeeDocument>) {}

    async addEmployee(employee: Employee): Promise<Employee> {
        const newEmployee = new this.employeeModel(employee);
        console.log(employee)
        return newEmployee.save();
    }

    async getAllEmployee(): Promise<Employee[]> {
        const employees = await this.employeeModel.find().exec();
        return employees;
    }

    async getEmployee(employeeId): Promise<Employee> {
        const employee = await this.employeeModel.findById(employeeId);
        return employee;
    }

    async updateEmployee(employeeId, employee: Employee): Promise<Employee> {
        const updatedEmployee = await this.employeeModel.findByIdAndUpdate(employeeId, employee, { new: true });
        return updatedEmployee;
    }

    async deleteEmployee(employeeId): Promise<any> {
        const deletedCustomer = await this.employeeModel.findByIdAndRemove(employeeId);
        return deletedCustomer;
    }
}