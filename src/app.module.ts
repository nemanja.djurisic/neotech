import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { EmployeeController } from './controllers/employee.controller';
import { EmployeeService } from './services/employee.service';
import { Employee, EmployeeSchema } from './models/employee.schema';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/Neotech',  { useNewUrlParser: true }),
    MongooseModule.forFeature([{name: Employee.name, schema: EmployeeSchema}])
  ],
  controllers: [AppController, EmployeeController],
  providers: [AppService, EmployeeService],
})
export class AppModule {}
