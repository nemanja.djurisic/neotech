import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Address } from './address.schema';
export type EmployeeDocument = Employee & Document;
@Schema()
export class Employee {
    @Prop({required:true})
    name: String;
    @Prop({required:true})
    email: String;
    @Prop({required:true})
    phone: String;
    @Prop({type: Address})
    address: Address;
    @Prop({required:true})
    employmentDate: String;
    @Prop({required:true})
    birthdate: String;
}
export const EmployeeSchema = SchemaFactory.createForClass(Employee)