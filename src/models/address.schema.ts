import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
@Schema()
export class Address {
  @Prop({ required: true })
  city: string;
  
  @Prop({ required: true })
  zipcode: number;
  
  @Prop({ required: true })
  AddressLineOne: string;
  
  @Prop()
  AddressLineTwo: string;
}
export const BodySchema = SchemaFactory.createForClass(Address);